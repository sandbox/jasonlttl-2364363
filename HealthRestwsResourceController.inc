<?php

/**
 * Example controller class for the health_restws_monitor resource.
 */
class HealthRestwsResourceController implements RestWSQueryResourceControllerInterface {

  /**
   * @see hook_entity_property_info()
   * @see RestWSResourceControllerInterface::propertyInfo()
   */
  public function propertyInfo() {
    return array(
      'name' => array(
        'type' => 'text',
        'label' => t('Name'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'group' => array(
        'type' => 'text',
        'label' => t('Group'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'description' => array(
        'type' => 'text',
        'label' => t('Description'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'status' => array(
        'type' => 'text',
        'label' => t('Status'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'message' => array(
        'type' => 'text',
        'label' => t('Message'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
    );
  }

  protected function init() {
    if (!is_array($this->monitors)) {
      $this->monitors = health_restws_load();
    }
  }

  protected function get($id) {
    $this->init();
    $item = $this->monitors[$id];
    return $item;
  }

  /**
   * @see RestWSResourceControllerInterface::wrapper()
   */
  public function wrapper($id) {
    $item = $this->get($id);
    $info = $this->propertyInfo();
    return entity_metadata_wrapper('health_restws_monitor', $item, array('property info' => $info));

  }

  /**
   * @see RestWSResourceControllerInterface::create()
   */
  public function create(array $values) {
    throw new RestWSException('Not implemented', 501);
  }

  /**
   * @see RestWSResourceControllerInterface::read()
   */
  public function read($id) {
    $item = $this->get($id);
    return $item;
  }

  /**
   * @see RestWSResourceControllerInterface::update()
   */
  public function update($id, array $values) {
    throw new RestWSException('Not implemented', 501);
  }

  /**
   * @see RestWSResourceControllerInterface::delete()
   */
  public function delete($id) {
    throw new RestWSException('Not implemented', 501);
  }

  /**
   * @see RestWSResourceControllerInterface::access()
   */
  public function access($op, $id) {
    return TRUE;
  }

  /**
   * @see RestWSResourceControllerInterface::resource()
   */
  public function resource() {
    return 'health_restws_monitor';
  }

  public function query($filters = array(), $meta_controls = array()) {
    $this->init();
    $monitors = array();
    foreach ($this->monitors as $id => $monitor) {
      $match = TRUE;
      foreach ($filters as $key => $val) {
        $match = $match && array_key_exists($key, $monitor) && ($monitor[$key] == $val);
      }
      if ($match) {
        $monitors[] = $id;
      }
    }
    return $monitors;
  }

  public function count($filters = array()) {

  }

  public function limit($client_limit = NULL) {

  }
}